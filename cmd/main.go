package main

import (
	"fmt"
	"time"

	log "bitbucket.org/menchshikov/zaplog"
)

func main() {
	c := log.NewConfig()
	//c.Logger.MaxSize = 10

	_ = log.NewLoggerWithConfig(c)
	fmt.Println(c)

	oldTime := time.Now()

	for i := 0; i < 1000000; i++ {
		log.Debug("test %8.d", i)
		log.Info("test %8.d", i)
		log.Warn("test %8.d", i)
		log.Error("test %8.d", i)
		log.DPanic("test %8.d", i)
		// log.Panic("test %8.d", i)
		// log.Fatal("test %8.d", i)
	}
	newTime := time.Now()
	log.Debug("Time: %v", newTime.Sub(oldTime))
}
