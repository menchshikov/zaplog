package zaplog

import (
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	Logr *zap.Logger
	//CanColorStderr = true
	//CanColorStdout = true
)

//const (
//	DEFAULT = "default"
//)

type Config struct {
	*lumberjack.Logger
	LogLevel   string
	UseConsole bool
}

func NewConfig() *Config {
	return &Config{
		&lumberjack.Logger{
			Filename:   "log",
			MaxSize:    50, // megabytes
			MaxBackups: 9,
			MaxAge:     30, // days
			Compress:   true,
			LocalTime:  true,
		},
		"debug",
		true,
	}
}

func NewLoggerWithConfig(opt *Config) error {
	zconfig := zap.NewProductionEncoderConfig()
	zconfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	zconfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.Format("2006/01/02 15:04:05.000 -0700"))
	}

	zconfig2 := zap.NewProductionEncoderConfig()
	zconfig2.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.Format("2006/01/02 15:04:05.000 -0700"))
	}

	w := zapcore.AddSync(opt.Logger)

	encoder := zapcore.NewConsoleEncoder(zconfig)
	encoder2 := zapcore.NewConsoleEncoder(zconfig2)
	atom := zap.NewAtomicLevelAt(zap.InfoLevel)
	atom.SetLevel(getZapLevel(opt.LogLevel))

	Logr = zap.L()
	if opt.UseConsole {
		Logr = zap.New(zapcore.NewTee(
			zapcore.NewCore(encoder2, w, atom),
			zapcore.NewCore(encoder, zapcore.AddSync(os.Stdout), atom),
		), zap.AddCaller(), zap.AddCallerSkip(1))
	} else {
		Logr = zap.New(zapcore.NewTee(
			zapcore.NewCore(encoder2, w, atom),
		), zap.AddCaller(), zap.AddCallerSkip(1))
	}
	zap.ReplaceGlobals(Logr)
	return nil
}

func NewLogger(logFile string, logLevel string, useConsole bool) {
	c := NewConfig()
	c.Filename = logFile
	c.LogLevel = logLevel
	c.UseConsole = useConsole
	_ = NewLoggerWithConfig(c)
	// curl -X PUT -d '{"level":"debug"}' localhost:1065/log_level
	// curl -X PUT -d '{"level":"info"}' localhost:1065/log_level
	// curl -X PUT -d '{"level":"info"}' localhost:1065/  # EXPECTED 404
	// curl -X GET localhost:1065/log_level
	// mux := http.NewServeMux()
	// mux.Handle("/log_level", atom)
	// // mux.Handle("/log", *Logr)
	// go http.ListenAndServe(":1065", mux)
}

func Debug(template string, args ...interface{}) {
	zap.S().Debugf(template, args...)
}

func Info(template string, args ...interface{}) {
	zap.S().Infof(template, args...)
}

func Warn(template string, args ...interface{}) {
	zap.S().Warnf(template, args...)
}

func Error(template string, args ...interface{}) {
	zap.S().Errorf(template, args...)
}

func DPanic(template string, args ...interface{}) {
	zap.S().DPanicf(template, args...)
}

func Panic(template string, args ...interface{}) {
	zap.S().Panicf(template, args...)
}

func Fatal(template string, args ...interface{}) {
	zap.S().Fatalf(template, args...)
}

func getZapLevel(logLevel string) zapcore.Level {
	switch logLevel {
	case "info":
		return zapcore.InfoLevel
	case "warn":
		return zapcore.WarnLevel
	case "debug":
		return zapcore.DebugLevel
	case "error":
		return zapcore.ErrorLevel
	case "fatal":
		return zapcore.FatalLevel
	default:
		return zapcore.InfoLevel
	}
}
